<?php
namespace Skrepr\Datagrid;

/**
 * Class Configuration
 *
 * @package Skrepr\Datagrid
 */
class Configuration
{
    /**
     * @var string
     */
    protected $viewPath;

    /**
     * Retrieves Datagrid's viewPath
     * @return string
     */
    public function getViewPath()
    {
        return $this->viewPath;
    }

    /**
     * Sets Datagrid's viewPath
     * @param $viewPath
     */
    public function setViewPath($viewPath)
    {
        $this->viewPath = $viewPath;
    }
}

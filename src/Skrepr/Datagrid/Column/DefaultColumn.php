<?php
namespace Skrepr\Datagrid\Column;

class DefaultColumn extends AbstractColumn
{
    protected $column;

    public function __construct($column)
    {
        $this->columns = func_get_args();
    }

    public function format($row)
    {
        $return = '';
        foreach ($this->columns as $column) {
            $return .= $row[$column] . ' ';
        }

        return rtrim($return);
    }
}

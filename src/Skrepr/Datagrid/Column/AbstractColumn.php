<?php
namespace Skrepr\Datagrid\Column;
use Skrepr\Datagrid\Datagrid;

/**
 * Class AbstractColumn
 *
 * @package Skrepr\Datagrid\Column
 */
abstract class AbstractColumn
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var boolean
     */
    protected $hideName;

    /**
     * Column options. Column options can
     * be used by standard Datagrid methods
     * or by custom Columns.
     *
     * @var array
     */
    protected $options;

    /**
     * @var Datagrid
     */
    protected $datagrid;

    /**
     * @param $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $datagrid
     */
    public function setDatagrid($datagrid)
    {
        $this->datagrid = $datagrid;
    }

    /**
     * @return Datagrid
     */
    public function getDatagrid()
    {
        return $this->datagrid;
    }

    /**
     * @return string
     */
    public function getNameTranslated()
    {
        $columnTranslator = $this->getDatagrid()->getConfiguration()->columnTranslator;
        if (is_callable($columnTranslator)) {
            return $columnTranslator->__invoke($this);
        }

        return $this->getName();
    }

    /**
     * @param $bHide
     */
    public function setHideName($bHide)
    {
        $this->hideName = (bool) $bHide;
    }

    /**
     * @return bool
     */
    public function isHideName()
    {
        return $this->hideName ? true : false;
    }

    /**
     * @param array $options
     */
    public function setOptions(array $options)
    {
        $this->options = $options;
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * @param $key
     * @param $value
     */
    public function setOption($key, $value)
    {
        $this->options[$key] = $value;
    }

    /**
     * Get column option by key.
     *
     * @param $key
     * @return null
     */
    public function option($key)
    {
        if (isset($this->options[$key])) {
            return $this->options[$key];
        }

        return null;
    }

    /**
     * @param $row
     * @return mixed
     */
    public abstract function format($row);
}
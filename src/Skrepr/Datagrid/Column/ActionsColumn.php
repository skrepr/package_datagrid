<?php
namespace Skrepr\Datagrid\Column;

use Skrepr\Datagrid\Exception;

/**
 * Class ActionsColumn
 *
 * @package Skrepr\Datagrid\Column
 */
class ActionsColumn extends AbstractColumn
{
    protected $closure;

    /**
     * @param $closure
     * @param array|null $options
     * @throws \Skrepr\Datagrid\Exception
     */
    public function __construct($closure, $options = array())
    {
        if (!is_callable($closure)) {
            throw new Exception('Parameter is not callable');
        }

        $this->closure = $closure;

        parent::setHideName(true);

        $this->setOptions($options);
    }

    /**
     * @param $key
     * @param $value
     * @throws \InvalidArgumentException
     */
    public function __set($key, $value)
    {
        switch ($key) {
            case 'closure':
                if (!is_callable($value)) {
                    throw new \InvalidArgumentException('Not a closure');
                }

                $this->closure = $value;
                break;
        }
    }

    /**
     * Returns column html
     *
     * @param $row
     * @return string
     */
    public function format($row)
    {
        $actions = $this->closure->__invoke($row);

        $html = '<div class="btn-group pull-right">';
        foreach ($actions as $action) {
            if (true !== $this->option('disableBtnClass')) {
                $class = 'class="btn';
            } else {
                $class = 'class="';
            }

            if (true === $this->option('bootstrap3small')) {
                $class .= ' btn-sm';
            }

            if (!empty($action['class'])) {
                $class .= ' ' . $action['class'];
            }

            if (array_key_exists('dropdown-items', $action)) {
                $class .= ' dropdown-toggle';

                $action['data-toggle'] = 'dropdown';
            }

            $class .= '" ';

            $href = isset($action['href']) ? 'href="' . $action['href'] . '"' : '';
            $html .= '<a ' . $href . ' ' . $class;

            $sysOptions = array('href', 'label', 'class', 'dropdown-items');
            $options = array();
            foreach ($action as $option => $value) {
                if (!in_array($option, $sysOptions)) {
                    $options[] = $option . '="' . $value . '"';
                }
            }

            $html .= implode(' ', $options);
            $html .= '>';

            $html .= $action['label'];

            $html .= '</a>';

            if (array_key_exists('dropdown-items', $action)) {
                $html .= '<ul class="dropdown-menu">';

                foreach ($action['dropdown-items'] as $item) {

                    if (isset($item['type']) && $item['type'] == 'divider') {
                        $html .= '<li class="divider"></li>';
                    } else {
                        $class = 'class="';

                        if (!empty($item['class'])) {
                            $class .= ' ' . $item['class'];
                        }

                        $class .= '"';

                        $href = isset($item['href']) ? 'href="' . $item['href'] . '"' : '';
                        $link = '<a ' . $href . ' ' . $class . ' ';

                        $options = [];
                        foreach ($item as $option => $value) {
                            if (!in_array($option, $sysOptions)) {
                                $options[] = $option . '="' . $value . '"';
                            }
                        }

                        $link .= implode(' ', $options);
                        $link .= '>' . $item['label'] . '</a>';

                        $html .= '<li>' . $link . '</li>';
                    }
                }

                $html .= '</ul>';
            }
        }
        $html .= '</div>';

        return $html;
    }
}
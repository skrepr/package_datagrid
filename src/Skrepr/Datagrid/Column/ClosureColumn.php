<?php
namespace Skrepr\Datagrid\Column;

/**
 * Class ClosureColumn
 *
 * @package Skrepr\Datagrid\Column
 */
class ClosureColumn extends AbstractColumn
{
    /**
     * @var callable
     */
    protected $closure;

    /**
     * @param $closure
     * @throws Exception
     */
    public function __construct($closure)
    {
        if (!is_callable($closure)) {
            throw new Exception('Parameter is not callable');
        }

        $this->closure = $closure;
    }

    /**
     * @param $row
     * @return mixed
     */
    public function format($row)
    {
        return $this->closure->__invoke($row);
    }
}
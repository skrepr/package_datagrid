<?php
namespace Skrepr\Datagrid\Column;

class DefaultActionsColumn extends AbstractColumn
{
    protected $actions;

    public function __construct($actions)
    {
        $this->actions = $actions;
    }

    public function format($row)
    {
        $html = '<div class="btn-group pull-right">';
        foreach ($this->actions as $key => $action) {

            if (true !== $this->option('disableBtnClass')) {
                $class = 'class="btn';
            } else {
                $class = 'class="';
            }

            if (!empty($action['class'])) {
                $class .= ' ' . $action['class'];
            }
            $class .= '"';

            $href = isset($action['href']) ? 'href="' . $action['href'] . '"' : '';

            // -- Micro templating
            preg_match_all('/{+(.*?)}+/', $href, $results);
            foreach ($results[1] as $res) {
                $href = str_replace('{{'.$res.'}}', $row[$res], $href);
            }
            // -- End

            $html .= '<a ' . $href . ' ' . $class;

            $sysOptions = array('href', 'label', 'class');
            $options = array();
            foreach ($action as $option => $value) {
                if (!in_array($option, $sysOptions)) {
                    $options[] = $option . '="' . $value . '"';
                }
            }

            $html .= implode(' ', $options);
            $html .= '>' . $action['label'] . '</a>';
        }
        $html .= '</div>';

        return $html;
    }
}

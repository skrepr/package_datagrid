<?php
namespace Skrepr\Datagrid;

use Skrepr\Datagrid\Datasource\AbstractSource;

/**
 * Class Datagrid
 *
 * @package Skrepr\Datagrid
 */
class Datagrid
{
    /**
     * @var AbstractSource
     */
    protected $datasource;

    /**
     * Datagrid html
     *
     * @var string
     */
    protected $html;

    /**
     * @var boolean
     */
    protected $calledRender;

    /**
     * Datagrid's unique id
     * When not set, the class name will be used
     *
     * Important: a unique id is needed when multiple
     * instances of a datagrid are present, as this id
     * is used to determine for which datagrid a XMLHttpRequest
     * is intended.
     *
     * @var string
     */
    protected $id;

    /**
     * Array with data to be encoded to json
     * @var array
     */
    protected $jsonArray;

    /**
     * @var Configuration
     */
    protected $configuration;

    /**
     * @var Configuration
     */
    protected static $defaultConfiguration;

    /**
     * @var string
     */
    protected $viewPath;

    /**
     * @var \Closure
     */
    protected $beforeRender;

    /**
     * @var array
     */
    protected $params;

    /**
     * @param $datasource
     * @return \Skrepr\Datagrid\Datagrid
     */
    public function __construct($datasource = null)
    {
        defined('DATAGRID_PATH')
          or define('DATAGRID_PATH', (getenv('DATAGRID_PATH')
            ? getenv('DATAGRID_PATH')
            : dirname(__DIR__ . '/../../../../')));

        $this->viewPath = DATAGRID_PATH . '/views/scripts/datagrid.phtml';

        if ($datasource instanceof AbstractSource) {
            $this->setDatasource($datasource);
        }

        if (method_exists($this, 'init')) {
            call_user_func_array(array($this, 'init'), func_get_args());
        }

        /**
         * Fix voor IE 'Back navigation caching'
         * (https://msdn.microsoft.com/en-us/library/dn265017%28v=vs.85%29.aspx)
         */
        header('Cache-Control: no-cache, max-age=0, must-revalidate, no-store');
    }

    /**
     * @param Datasource\AbstractSource $datasource
     * @return $this
     */
    public function setDatasource(Datasource\AbstractSource $datasource)
    {
        $this->datasource = $datasource;
        return $this;
    }

    /**
     * @return AbstractSource
     */
    public function getDatasource()
    {
        return $this->datasource;
    }

    /**
     * @param $name
     * @param $column
     * @param array $options
     * @throws Exception
     * @return $this
     */
    public function addColumn($name, $column, array $options = null)
    {
        if (null === $this->getDatasource()) {
            throw new Exception('No datasource set');
        }

        if ($column instanceof \Closure) {
            $column = new Column\ClosureColumn($column);
        }

        $column->setName($name);
        $column->setDatagrid($this);
        $options && $column->setOptions($options);
        $this->getDatasource()->addColumn($column);
        return $this;
    }

    /**
     * Remove a column
     *
     * @param $name
     * @return $this
     */
    public function removeColumn($name)
    {
        $this->getDatasource()->removeColumn($name);

        return $this;
    }

    /**
     * @param $name
     * @return mixed
     */
    public function getColumn($name)
    {
        return $this->getDatasource()->getColumn($name);
    }

    /**
     * @throws Exception
     * @return $this
     */
    public function render()
    {
        if (null === $this->getDatasource()) {
            throw new Exception('No datasource set');
        }

        $this->beforeRender();
        $this->calledRender(true);

        $headerVal = null;

        // Try to get it from the $_SERVER array first
        $temp = 'HTTP_X_REQUESTED_WITH';
        if (isset($_SERVER[$temp])) {
            $headerVal = $_SERVER[$temp];
        }

        // This seems to be the only way to get the Authorization header on
        // Apache
        if (function_exists('apache_request_headers')) {
            $headers = apache_request_headers();
            if (isset($headers['X_REQUESTED_WITH'])) {
                $headerVal = $headers['X_REQUESTED_WITH'];
            }
            $header = strtolower('X_REQUESTED_WITH');
            foreach ($headers as $key => $value) {
                if (strtolower($key) == $header) {
                    $headerVal = $value;
                }
            }
        }

        if (($headerVal == 'XMLHttpRequest' || !empty($_GET['datagridtest'])) && $this->compareId()) {
            $this->renderJson();
        } else {
            $this->renderHtml();
        }

        return $this;
    }

    /**
     * Render html portion of datagrid
     *
     * @throws Exception
     */
    public function renderHtml()
    {
        ob_start();

        $configuration = $this->getConfiguration();

        if (null === $configuration or null === $configuration->getViewPath()) {
            include $this->viewPath;
        } else {
            include $configuration->getViewPath();
        }

        $this->html = ob_get_clean();
    }

    /**
     * Render json portion of datagrid
     *
     * @throws Exception
     * @return string json
     */
    public function renderJson()
    {
        header('Content-type: application/json');
        echo json_encode($this->getJsonData());
        exit();
    }

    /**
     * Validates params array
     * Adds extra params from datasource
     *
     * @param $params
     * @return mixed
     */
    public function validateParams($params)
    {
        $params = array_merge($params, $this->getDatasource()->getParams());

        return $params;
    }

    /**
     * @param $param
     * @param $value
     */
    public function setParam($param, $value)
    {
        $this->params[$param] = $value;
    }

    /**
     * return params
     *
     * @return mixed
     */
    public function getParams()
    {
        if (null === $this->params) {
            $this->params = $this->validateParams($_GET);
        }

        return $this->params;
    }

    /**
     * @param $params
     */
    public function setParams($params)
    {
        $this->params = $params;
    }

    /**
     * @param $param
     * @return mixed
     */
    public function getParam($param)
    {
        if (array_key_exists($param, $this->getParams())) {
            return $this->params[$param];
        }
    }

    /**
     * @param $param
     * @return mixed
     */
    public function param($param)
    {
        return $this->getParam($param);
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public function getJsonData()
    {
        $return = array();

        if (!($params = $this->getParams())) {
            $return['status'] = 'error';
        } else {
            $total = $this->getDatasource()->totalCount($params);
            $pages = (int) ceil($total / (int) $params['pageSize']);

            $return['data'] = $this->getDatasource()->listRows($params);
            $return['count'] = $total;
            $return['page'] = (int) $params['page'];
            $return['perpage'] = (int) $params['pageSize'];
            $return['pages'] = $pages;
            $return['status'] = 'success';
        }

        $this->jsonArray = $return;

        return $this->jsonArray;
    }

    /**
     * @param null $called
     * @return bool
     */
    public function calledRender($called = null)
    {
        if (null === $called) {
            return $this->calledRender;
        }

        $this->calledRender = (bool) $called;

        return true;
    }

    /**
     * @return string
     */
    public function getId()
    {
        if (null === $this->id) {
            $calledClass = explode('\\', get_called_class());
            $this->setId(end($calledClass));
        }

        return $this->id;
    }

    /**
     * @param $did
     * @return void
     */
    public function setId($did)
    {
        $this->id = $did;
    }

    /**
     * @return bool
     */
    public function compareId()
    {
        $datagridId = isset($_GET['skrdatagridid']) ? $_GET['skrdatagridid'] : false;
        return ($this->getId() === $datagridId);
    }

    /**
     * Generates datagrid
     *
     * @return string
     */
    public function __toString()
    {
        if (!$this->calledRender()) {
            return 'You need to call Datagrid#render() first.';
        }

        return $this->html;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return sprintf(
            "//%s%s",
            $_SERVER['HTTP_HOST'],
            $_SERVER['REQUEST_URI']
        );
    }

    /**
     * Set Datagrid's configuration object
     *
     * @param $configuration
     */
    public function setConfiguration($configuration)
    {
        $this->configuration = $configuration;
    }

    /**
     * Set Datagrid's default configuration object
     *
     * @param Configuration $configuration
     */
    public static function setDefaultConfiguration(Configuration $configuration)
    {
        self::$defaultConfiguration = $configuration;
    }

    /**
     * Retrieves Datagrid's configuration object
     *
     * @return Configuration
     */
    public function getConfiguration()
    {
        if (null !== $this->configuration) {
            return $this->configuration;
        }

        if (null !== self::$defaultConfiguration) {
            return self::$defaultConfiguration;
        }

        return null;
    }

    public function tryGetConfiguration()
    {
        $configuration = $this->getConfiguration();

        if (null === $configuration) {
            throw new Exception('Configuration object not found.');
        }

        return $configuration;
    }

    public function beforeRender($closure = null)
    {
        if (null !== $closure) {
            $this->beforeRender = $closure;
        } elseif ($this->beforeRender && $this->compareId()) {
            call_user_func_array($this->beforeRender, array($this));
        }
    }
}

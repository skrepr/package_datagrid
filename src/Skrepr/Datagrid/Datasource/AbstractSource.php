<?php
namespace Skrepr\Datagrid\Datasource;

use Skrepr\Datagrid\Column\AbstractColumn;
use Skrepr\Datagrid\Exception;

/**
 * Class AbstractSource
 *
 * @package Skrepr\Datagrid\Datasource
 */
abstract class AbstractSource
{
    /**
     * @var array
     */
    protected $columns = array();

    /**
     * @var array
     */
    protected $params = array();

    abstract public function listRows(array $params);

    abstract public function totalCount(array $params);

    /**
     * @param \Skrepr\Datagrid\Column\AbstractColumn
     * @return $this
     */
    public function addColumn(AbstractColumn $column)
    {
        $this->columns[] = $column;
        return $this;
    }

    /**
     * @return array
     */
    public function getColumns()
    {
        return $this->columns;
    }

    /**
     * Remove a column
     * Use datagrid->removeColumn()
     *
     * @param $name
     * @return this
     */
    public function removeColumn($name)
    {
        foreach ($this->columns as $key => $column) {
            if ($column->getName() == $name) {
                unset($this->columns[$key]);
            }
        }

        return $this;
    }

    /**
     * @param $name
     * @return mixed
     * @throws \Skrepr\Datagrid\Exception
     */
    public function getColumn($name)
    {
        foreach ($this->columns as $column) {
            if ($column->getName() == $name) {
                return $column;
            }
        }

        throw new Exception('Column not found');
    }

    /**
     * Use this method to set extra params for use
     * in datasource
     *
     * @param $key
     * @param $value
     * @return $this
     */
    public function setParam($key, $value)
    {
        $this->params[$key] = $value;
        return $this;
    }

    /**
     * @return array
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * @param $key
     * @return null
     */
    public function param($key)
    {
        return array_key_exists($key, $this->params) ? $this->params[$key] : null;
    }
}
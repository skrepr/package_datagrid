<?php
namespace Skrepr\Datagrid\Datasource;

use Skrepr\Datagrid\Exception;

/**
 * Class DefaultSource
 *
 * @package Skrepr\Datagrid\Datasource
 */
class DefaultSource extends AbstractSource
{
    /**
     * @var object
     */
    protected $source;

    /**
     * @param $source
     * @throws \Skrepr\Datagrid\Exception
     */
    public function __construct($source)
    {
        if (!is_object($source)) {
            throw new Exception('DefaultSource needs an object, ' . gettype($source) . ' given instead.');
        }

        if (!method_exists($source, 'datagridPage') || !method_exists($source, 'datagridTotalCount')) {
            throw new Exception('You need to implement both datagridPage() and datagridTotalCount()');
        }

        $this->source = $source;
    }

    /**
     * @param array $params
     * @return array
     */
    public function listRows(array $params)
    {
        $rows = $this->source->datagridPage($params);

        $entities = array();
        foreach ($rows as $row) {
            $entity = array();
            foreach ($this->getColumns() as $column) {
                $entity[$column->getName()] = $column->format($row);
            }

            $entities[] = $entity;
        }

        return $entities;
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function totalCount(array $params)
    {
        return $this->source->datagridTotalCount($params);
    }
}

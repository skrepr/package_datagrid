<?php
namespace Skrepr\Datagrid\Datasource;

class ArraySource extends AbstractSource
{
    protected $array;

    /**
     * @param array $array the array
     */
    public function __construct(array $array)
    {
        $this->array = $array;
    }

    /**
     * @param array $params
     * @return array
     */
    public function listRows(array $params)
    {
        $array = $this->array;

        /**
         * @todo search array ($params['q'])
         * @todo split array ($params['page'] & $params['pageSize'])
         */

        $entities = array();
        foreach ($array as $item) {
            $entity = array();
            foreach ($this->getColumns() as $column) {
                $entity[$column->getName()] = $column->format($item);
            }

            $entities[] = $entity;
        }

        return $entities;
    }

    /**
     * @param array $params
     * @return int
     */
    public function totalCount(array $params)
    {
        return count($this->array);
    }
}
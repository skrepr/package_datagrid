<?php
namespace Skrepr\Datagrid\Datasource;

use Skrepr\Datagrid\Exception;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

/**
 * Class DoctrineSource
 *
 * @package Skrepr\Datagrid\Datasource
 */
class DoctrineSource extends AbstractSource
{
    /**
     * EntityRepository class name
     */
    const ER = 'Doctrine\ORM\EntityRepository';

    /**
     * @var EntityRepository
     */
    protected $entityRepository;

    /**
     * @var QueryBuilder
     */
    protected $queryBuilder;

    /**
     * @param $entityRepository
     * @throws \Skrepr\Datagrid\Exception
     */
    public function __construct($entityRepository)
    {
        if (!is_object($entityRepository)) {
            throw new Exception('Need instance of ' . self::ER);
        }

        $this->entityRepository = $entityRepository;
        $this->queryBuilder = $this->entityRepository->createQueryBuilder('t');
    }

    /**
     * @param array $params
     */
    protected function datagrid(array $params)
    {
        if (method_exists($this->entityRepository, 'datagridParams')) {
            $this->entityRepository->datagridParams($this, $params);
        }
    }

    /**
     * @param array $params
     * @return mixed
     */
    protected function datagridPage(array $params)
    {
        $this->datagrid($params);

        // limit
        if (isset($params['page'], $params['pageSize'])) {
            $this->queryBuilder->setFirstResult(($params['page'] - 1) * $params['pageSize']);
            $this->queryBuilder->setMaxResults($params['pageSize']);
        }

        return $this->queryBuilder->getQuery()->getResult();
    }

    /**
     * @param array $params
     * @return array
     */
    public function listRows(array $params)
    {
        $rows = $this->datagridPage($params);

        $entities = array();
        foreach ($rows as $row) {
            $entity = array();
            foreach ($this->getColumns() as $column) {
                $entity[$column->getName()] = $column->format($row);
            }

            $entities[] = $entity;
        }

        return $entities;
    }

    /**
     * @param array $params
     * @return int
     */
    public function totalCount(array $params)
    {
        $this->datagrid($params);

        $qb = clone $this->queryBuilder;
        $result = $qb->select('COUNT(t)')->getQuery()->getScalarResult();

        return array_sum(array_map(function ($item) {
            return $item[1];
        }, $result));
    }

    /**
     * @return QueryBuilder
     */
    public function getQueryBuilder()
    {
        return $this->queryBuilder;
    }
}

```php
$configuration = new \Skrepr\Datagrid\Configuration();

$configuration->setParamsGetter(function() {
    return Zend_Controller_Front::getInstance()->getRequest()->getParams();
});

$configuration->howRenderJson(function($data) {
    $jsonHelper = Zend_Controller_Action_HelperBroker::getStaticHelper('json');
    $jsonHelper->direct($data);
});

$configuration->howRenderHtml(function($datagrid) {
    $view = new \Zend_View();
    $view->setScriptPath(APPLICATION_PATH . '/views/scripts');

    $view->columns = $datagrid->getDatasource()->getColumns();
    $view->datagrid = $datagrid;
    return $view->render('datagrid.phtml');
});

\Skrepr\Datagrid\Datagrid::setDefaultConfiguration($configuration);
```
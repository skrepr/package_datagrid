/*
 * Fuel UX Datagrid
 * https://github.com/ExactTarget/fuelux
 *
 * Copyright (c) 2012 ExactTarget
 * Licensed under the MIT license.
 */

!function ($) { "use strict";

    // DATAGRID CONSTRUCTOR AND PROTOTYPE

    var Datagrid = function (element, options) {
        this.$element = $(element);
        this.$thead = this.$element.find('thead');
        this.$tfoot = this.$element.find('tfoot');
        this.$footer = this.$element.find('tfoot th');
        this.$footerchildren = this.$footer.children().show().css('visibility', 'hidden');
        this.$topheader = this.$element.find('thead th');
        this.$searchcontrol = this.$element.find('.search');
        this.$filtercontrol = this.$element.find('.filter');
        this.$pagesize = this.$element.find('.grid-pagesize');
        this.$pageinput = this.$element.find('.grid-pager input');
        this.$pagedropdown = this.$element.find('.grid-pager .dropdown-menu');
        this.$prevpagebtn = this.$element.find('.grid-prevpage');
        this.$nextpagebtn = this.$element.find('.grid-nextpage');
        this.$pageslabel = this.$element.find('.grid-pages');
        this.$countlabel = this.$element.find('.grid-count');
        this.$startlabel = this.$element.find('.grid-start');
        this.$endlabel = this.$element.find('.grid-end');

        this.$tbody = $('<tbody>').insertAfter(this.$thead);
        this.$colheader = $('<tr>').appendTo(this.$thead);

        this.options = $.extend(true, {}, $.fn.datagrid.defaults, options);

        if (this.$pagesize.hasClass('select')) {
            this.options.dataOptions.pageSize = parseInt(this.$pagesize.select('selectedItem').value, 10);
        } else {
            this.options.dataOptions.pageSize = parseInt(this.$pagesize.val(), 10);
        }

        this.columns = this.options.dataSource.columns();

        this.$nextpagebtn.on('click', $.proxy(this.next, this));
        this.$prevpagebtn.on('click', $.proxy(this.previous, this));
        this.$searchcontrol.on('searched cleared', $.proxy(this.searchChanged, this));
        this.$filtercontrol.on('changed', $.proxy(this.filterChanged, this));
        this.$colheader.on('click', 'th', $.proxy(this.headerClicked, this));

        if (this.$pagesize.hasClass('select')) {
            this.$pagesize.on('changed', $.proxy(this.pagesizeChanged, this));
        } else {
            this.$pagesize.on('change', $.proxy(this.pagesizeChanged, this));
        }

        this.$pageinput.on('change', $.proxy(this.pageChanged, this));

        this.renderColumns();

        if (this.options.stretchHeight) this.initStretchHeight();

        this.renderData();
    };

    Datagrid.prototype = {

        constructor: Datagrid,

        renderColumns: function () {
            var self = this;

            this.$footer.attr('colspan', this.columns.length);
            this.$topheader.attr('colspan', this.columns.length);

            var colHTML = '';

            $.each(this.columns, function (index, column) {
                colHTML += '<th data-property="' + column.property + '"';
                if (column.sortable) colHTML += ' class="sortable"';
                colHTML += '>' + column.label + '</th>';
            });

            self.$colheader.append(colHTML);
        },

        updateColumns: function ($target, direction) {
            var className = (direction === 'asc') ? 'icon-chevron-up' : 'icon-chevron-down';
            this.$colheader.find('i').remove();
            this.$colheader.find('th').removeClass('sorted');
            $('<i>').addClass(className).appendTo($target);
            $target.addClass('sorted');
        },

        updatePageDropdown: function (data) {
            var pageHTML = '';

            for (var i = 1; i <= data.pages; i++) {
                pageHTML += '<li><a>' + i + '</a></li>';
            }

            this.$pagedropdown.html(pageHTML);
        },

        updatePageButtons: function (data) {
            if (data.page === 1) {
                this.$prevpagebtn.attr('disabled', 'disabled');
            } else {
                this.$prevpagebtn.removeAttr('disabled');
            }

            if (data.page === data.pages) {
                this.$nextpagebtn.attr('disabled', 'disabled');
            } else {
                this.$nextpagebtn.removeAttr('disabled');
            }
        },

        renderData: function () {
            var self = this;

            this.$tbody.html(this.placeholderRowHTML(this.options.loadingHTML));

            this.options.dataSource.data(this.options.dataOptions, function (data) {
                var itemdesc = (data.count === 1) ? self.options.itemText : self.options.itemsText;
                var rowHTML = '';

                self.$footerchildren.css('visibility', function () {
                    return (data.count > 0) ? 'visible' : 'hidden';
                });

                self.$pageinput.val(data.page);
                self.$pageslabel.text(data.pages);
                self.$countlabel.text(data.count + ' ' + itemdesc);
                self.$startlabel.text(data.start);
                self.$endlabel.text(data.end);

                self.updatePageDropdown(data);
                self.updatePageButtons(data);

                $.each(data.data, function (index, row) {
                    rowHTML += '<tr>';
                    $.each(self.columns, function (index, column) {
                        rowHTML += '<td>' + row[column.property] + '</td>';
                    });
                    rowHTML += '</tr>';
                });

                if (!rowHTML) rowHTML = self.placeholderRowHTML('0 ' + self.options.itemsText);

                self.$tbody.html(rowHTML);
                self.stretchHeight();

                self.$element.trigger('loaded');
            });

        },

        placeholderRowHTML: function (content) {
            return '<tr><td style="text-align:center;padding:20px;border-bottom:none;" colspan="' +
                this.columns.length + '">' + content + '</td></tr>';
        },

        headerClicked: function (e) {
            var $target = $(e.target);
            if (!$target.hasClass('sortable')) return;

            var direction = this.options.dataOptions.sortDirection;
            var sort = this.options.dataOptions.sortProperty;
            var property = $target.data('property');

            if (sort === property) {
                this.options.dataOptions.sortDirection = (direction === 'asc') ? 'desc' : 'asc';
            } else {
                this.options.dataOptions.sortDirection = 'asc';
                this.options.dataOptions.sortProperty = property;
            }

            this.options.dataOptions.pageIndex = 0;
            this.updateColumns($target, this.options.dataOptions.sortDirection);
            this.renderData();
        },

        pagesizeChanged: function (e, pageSize) {
            if(pageSize) {
                this.options.dataOptions.pageSize = parseInt(pageSize.value, 10);
            } else {
                this.options.dataOptions.pageSize = parseInt($(e.target).val(), 10);
            }

            this.options.dataOptions.pageIndex = 0;
            this.renderData();
        },

        pageChanged: function (e) {
            this.options.dataOptions.pageIndex = parseInt($(e.target).val(), 10) - 1;
            this.renderData();
        },

        searchChanged: function (e, search) {
            this.options.dataOptions.search = search;
            this.options.dataOptions.pageIndex = 0;
            this.renderData();
        },

        filterChanged: function (e, filter) {
            this.options.dataOptions.filter = filter;
            this.renderData();
        },

        previous: function () {
            this.options.dataOptions.pageIndex--;
            this.renderData();
        },

        next: function () {
            this.options.dataOptions.pageIndex++;
            this.renderData();
        },

        reload: function () {
            this.options.dataOptions.pageIndex = 0;
            this.renderData();
        },

        initStretchHeight: function () {
            this.$gridContainer = this.$element.parent();

            this.$element.wrap('<div class="datagrid-stretch-wrapper">');
            this.$stretchWrapper = this.$element.parent();

            this.$headerTable = $('<table>').attr('class', this.$element.attr('class'));
            this.$footerTable = this.$headerTable.clone();

            this.$headerTable.prependTo(this.$gridContainer).addClass('datagrid-stretch-header');
            this.$thead.detach().appendTo(this.$headerTable);

            this.$sizingHeader = this.$thead.clone();
            this.$sizingHeader.find('tr:first').remove();

            this.$footerTable.appendTo(this.$gridContainer).addClass('datagrid-stretch-footer');
            this.$tfoot.detach().appendTo(this.$footerTable);
        },

        stretchHeight: function () {
            if (!this.$gridContainer) return;

            this.setColumnWidths();

            var targetHeight = this.$gridContainer.height();
            var headerHeight = this.$headerTable.outerHeight();
            var footerHeight = this.$footerTable.outerHeight();
            var overhead = headerHeight + footerHeight;

            this.$stretchWrapper.height(targetHeight - overhead);
        },

        setColumnWidths: function () {
            if (!this.$sizingHeader) return;

            this.$element.prepend(this.$sizingHeader);

            var $sizingCells = this.$sizingHeader.find('th');
            var columnCount = $sizingCells.length;

            function matchSizingCellWidth(i, el) {
                if (i === columnCount - 1) return;
                $(el).width($sizingCells.eq(i).width());
            }

            this.$colheader.find('th').each(matchSizingCellWidth);
            this.$tbody.find('tr:first > td').each(matchSizingCellWidth);

            this.$sizingHeader.detach();
        }
    };


    // DATAGRID PLUGIN DEFINITION

    $.fn.datagrid = function (option) {
        return this.each(function () {
            var $this = $(this);
            var data = $this.data('datagrid');
            var options = typeof option === 'object' && option;

            if (!data) $this.data('datagrid', (data = new Datagrid(this, options)));
            if (typeof option === 'string') data[option]();
        });
    };

    $.fn.datagrid.defaults = {
        dataOptions: { pageIndex: 0, pageSize: 10 },
        loadingHTML: '<div class="progress progress-striped active" style="width:50%;margin:auto;"><div class="bar" style="width:100%;"></div></div>',
        itemsText: 'items',
        itemText: 'item'
    };

    $.fn.datagrid.Constructor = Datagrid;

}(window.jQuery);


/*
 * Fuel UX Search
 * https://github.com/ExactTarget/fuelux
 *
 * Copyright (c) 2012 ExactTarget
 * Licensed under the MIT license.
 */

!function ($) { "use strict";

    // SEARCH CONSTRUCTOR AND PROTOTYPE

    var Search = function (element, options) {
        this.$element = $(element);
        this.options = $.extend({}, $.fn.search.defaults, options);

        this.$button = this.$element.find('button')
            .on('click', $.proxy(this.buttonclicked, this));

        this.$input = this.$element.find('input')
            .on('keydown', $.proxy(this.keypress, this))
            .on('keyup', $.proxy(this.keypressed, this));

        this.$icon = this.$element.find('i');
        this.activeSearch = '';
    };

    Search.prototype = {

        constructor: Search,

        search: function (searchText) {
            this.$icon.attr('class', 'icon-remove');
            this.activeSearch = searchText;
            this.$element.trigger('searched', searchText);
        },

        clear: function () {
            this.$icon.attr('class', 'icon-search');
            this.activeSearch = '';
            this.$input.val('');
            this.$element.trigger('cleared');
        },

        action: function () {
            var val = this.$input.val();
            var inputEmptyOrUnchanged = val === '' || val === this.activeSearch;

            if (this.activeSearch && inputEmptyOrUnchanged) {
                this.clear();
            } else if (val) {
                this.search(val);
            }
        },

        buttonclicked: function (e) {
            e.preventDefault();
            if ($(e.currentTarget).is('.disabled, :disabled')) return;
            this.action();
        },

        keypress: function (e) {
            if (e.which === 13) {
                e.preventDefault();
            }
        },

        keypressed: function (e) {
            var val, inputPresentAndUnchanged;

            if (e.which === 13) {
                e.preventDefault();
                this.action();
            } else {
                val = this.$input.val();
                inputPresentAndUnchanged = val && (val === this.activeSearch);
                this.$icon.attr('class', inputPresentAndUnchanged ? 'icon-remove' : 'icon-search');
            }
        },

        disable: function () {
            this.$input.attr('disabled', 'disabled');
            this.$button.addClass('disabled');
        },

        enable: function () {
            this.$input.removeAttr('disabled');
            this.$button.removeClass('disabled');
        }

    };


    // SEARCH PLUGIN DEFINITION

    $.fn.search = function (option) {
        return this.each(function () {
            var $this = $(this);
            var data = $this.data('search');
            var options = typeof option === 'object' && option;

            if (!data) $this.data('search', (data = new Search(this, options)));
            if (typeof option === 'string') data[option]();
        });
    };

    $.fn.search.defaults = {};

    $.fn.search.Constructor = Search;


    // SEARCH DATA-API

    $(function () {
        $('body').on('mousedown.search.data-api', '.search', function () {
            var $this = $(this);
            if ($this.data('search')) return;
            $this.search($this.data());
        });
    });

}(window.jQuery);

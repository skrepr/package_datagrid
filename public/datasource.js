/*
 * Fuel UX Data components - static data source
 * https://github.com/ExactTarget/fuelux-data
 *
 * Copyright (c) 2012 ExactTarget
 * Licensed under the MIT license.
 */

(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        define(['underscore'], factory);
    } else {
        root.DynamicDataSource = factory();
    }
}(this, function () {

    var DynamicDataSource = function (options) {
        this._formatter = options.formatter;
        this._columns = options.columns;
        this._url = options.url;
        this._id = options.id;
    };

    DynamicDataSource.prototype = {

        columns: function () {
            return this._columns;
        },

        data: function (options, callback) {
            var self = this;

            var url = this._url;

            url += '?skrdatagridid=' + this._id;

            if (typeof options.search !== 'undefined') {
                url += '&q=' + options.search;
            }

            if (typeof options.sortDirection !== 'undefined') {
                url += '&sortDirection=' + options.sortDirection;
            }

            if (typeof options.sortProperty !== 'undefined') {
                url += '&sortProperty=' + options.sortProperty;
            }

            url += '&pageSize=' + options.pageSize;
            url += '&page=' + (options.pageIndex + 1);

            $.get(url, function(responseJSON) {
                var data = responseJSON.data,
                    count = responseJSON.count,
                    startIndex = (responseJSON.page - 1) * responseJSON.perpage,
                    endIndex = startIndex + responseJSON.perpage,
                    end = (endIndex > count) ? count : endIndex,
                    pages = responseJSON.pages,
                    page = responseJSON.page,
                    start = startIndex + 1;

                if (self._formatter) {
                    self._formatter(data);
                }

                callback({
                    data: data,
                    start: start,
                    end: end,
                    count: count,
                    pages: pages,
                    page: page
                });
            });
        }
    };

    return DynamicDataSource;
}));